## Testing repo

This repo is for testing how commits with
[trailers](https://git.wiki.kernel.org/index.php/CommitMessageConventions)
are displayed in the Gitlab UI.

It looks like links followed by emails in a commit message will cause
Gitlab to eat a newline.

Example: 

https://gitlab.com/mandarg/golang-sys/commit/1b73967140201b02a8420ee8555ed00a432e97c0
looks weird, whereas the same commit looks fine in Github
https://github.com/golang/sys/commit/1b73967140201b02a8420ee8555ed00a432e97c0 (just to illustrate that the commit message is fine)
